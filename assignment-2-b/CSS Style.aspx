﻿<%@ Page Title="CSS Style" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CSS Style.aspx.cs" Inherits="assignment_2_b.CSS_Style" %>

<asp:Content ContentPlaceHolderID="courseOverview" runat="server">

    <div class="courseOverview">
        <h1>Styling Links or Anchor tag in CSS</h1>
        <p> Anchor tags in HTML are use to connect or link one page to another. In CSS we use different properties to style the links based on how we want it. For example links that are used for our menu navigation. Links are also style depending on what kind of state they are such as active, hover, and visited.</p>
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="mysampleCode" runat="server">
    <div class="col-md-12">
    <h2>My Sample Code</h2>
    <ASP:CodeBox ID="my_css_code" runat="server" Code="my_css"></ASP:CodeBox>
    <img class="sample-img" src="images/stylingLinks.jpg" alt="images of share icon styled in CSS">
    <p> The code shows how to style sharing social media icons. The properties I set for this are color, text-decoration, font size, and the padding so I can make sure there's a space between each icon. If I hover the icon it will turn into light blue color. The screenshot shows how it looks like when I hover the icon.</p>
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="SampleCode" runat="server">
    <div class="col-md-12 class=">
    <h2>In-Class Example</h2>
    <ASP:CodeBox ID="teacher_css_code" runat="server" Code="teacher_css" SkinID="code-dark"></ASP:CodeBox>
    <img class="sample-img" src="images/links.jpg" alt="exmple image of styled links">
    <p>The code above are different examples on how to style a link  on CSS. It will change its color based on what you set in different state. Take note, that when setting a style proery for any link state, it should be in the right order.</p> 
    <p>This example is from <a href="https://www.w3schools.com/css/css_link.asp">W3schools</a></p>
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="usefulLinks" runat="server">
            <h2>Other Resources for this topic </h2>
            <ul class="usefulLinks">
                <li><h3><a href="https://developer.mozilla.org/en-US/docs/Learn/CSS/Styling_text/Styling_links" target="_blank">Styling LInks</a></h3></li>
                <li><h3><a href="https://www.smashingmagazine.com/2010/02/the-definitive-guide-to-styling-web-links/" target="_blank">The Definitive Guide To Styling Links With CSS</a></h3></li>
                <li><h3><a href="https://www.lifewire.com/styling-links-with-css-3466838" target="_blank">How to Style Links With CSS</a></h3></li>
            </ul>
</asp:Content>

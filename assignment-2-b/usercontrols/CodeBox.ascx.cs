﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace assignment_2_b.usercontrols
{
    public partial class CodeBox : System.Web.UI.UserControl
    {
        public string Code
        {
            get { return (string)ViewState["Code"]; }
            set { ViewState["Code"] = value; }

        }

       
        DataView CreateCodeSource()
        {
            DataTable codedata = new DataTable();

            //debug.InnerHtml = Code + " Sample Code";

            DataColumn idx_col = new DataColumn();

            DataColumn code_col = new DataColumn();

            DataRow coderow;

            idx_col.ColumnName = "Line";
            idx_col.DataType = System.Type.GetType("System.Int32");

            codedata.Columns.Add(idx_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            codedata.Columns.Add(code_col);

            List<string> codetoserve;

            if (Code=="my_css")
            {
                //if I want to serve css code i use this
                codetoserve = new List<string>(new string[]{
                    ".share-socials a{ /* .share-socials is the ",
                    "class assign for this set of icons link*/",
                    "~~color:#2d2d2d; /* DARK GREY COLOR FOR ",
                    "~~~DEFAULT STATE */",
                    "~~text - decoration:none; ",
                    "~~~/* WHEN USING AN ANCHOR TAG, IT USUALLY ",
                    "~~~COME AS AN UNDERLINED LINK. TO REMOVE ",
                    "~~~THE UNDERLINE, WE SET THE TEXT ",
                    "~~~DECORATION TO NONE.*/",
                    "~~font - size: 2em;",
                    "~~padding - right: 18px; /* SPACE ",
                    "~~~BETWEEN ICONS */",
                    "}",
                    " ",
                    ".share-socials a:hover{",
                    "~~color:#65b6de; /* LIGHT BLUE COLOR FOR ",
                    "~~~HOVER STATE */",
                    "}"
                });
            }
            else if(Code=="my_js")
            { 
                //if I want to serve js code i use this
                codetoserve = new List<string>(new string[]{
                    "var objectName = {}; // SYNTAX FOR ",
                    "JAVASCRIPT OBJECT",
                    "~// Now we will make an object ",
                    "called myObject",
                    " ",
                    "var myObject = {",
                    "~~name: \"Enrina\",",
                    "~~age: 19,",
                    "~~gender: Female",
                    "~~profile: function(){",
                    "~~~~alert( \"Hi my name is \" + ",
                    "~~~~myObject.name + \", \" + myObject.gender ",
                    "~~~~+ \"and \" + myObject.age ",
                    "~~~~+ \" of age!\")",
                    "~~}",
                    "}",
                    "//OUTPUT WILL BE ON ALERTBOX ",
                    "WITH CONCATENATED STRING",
                    "myObject.profile();"
                });
            }

            else if (Code == "teacher_js")
            {
                codetoserve = new List<string>(new string[] {
                    " var objectName = {}; // SYNTAX FOR JAVASCRIPT OBJECT",
                    "~// Now we will make an object called myObject",
                    " ",
                    "var myCat = {",
                    "~~name: \"Chester\",",
                    "~~age: 6,",
                    "~~breed: \"Maine Coon\"",
                    "~~purr: function(){",
                    "~~~~alert(\"Prrrrrrrrrrrrrrrrrrrrrrrrr\");",
                    "~~}",
                    "}",
                    "//OUTPUT WILL BE ON ALERTBOX",
                    "myCat.purr();"
                });
            }

            else if (Code == "my_sql")
            {
                codetoserve = new List<string>(new string[] {
                "--SAMPLE QUERY FOR LEFT JOIN",

                  "SELECT invoice_amount, invoice_description,",
                  "clientlname || ', ' || clientfname as fullname",
                  "FROM clients",
                  "LEFT JOIN invoices",
                  "ON clients.clientid = invoices.clientid",
                  "WHERE invoice_amount > 500 or",
                  "invoice_description like 'replacement'",
                  "ORDER BY invoices.clientid asc;"
                });
            }

            else if(Code == "teacher_sql")
            {
                codetoserve = new List<string>(new string[] {
                "--SYNTAX FOR LEFT JOIN",
                "SELECT column_name1,",
                "column_name2",
                "FROM table_name",
                "LEFT JOIN table_name1",
                "ON table_name1.column_name",
                "= table_name1.column_name",
                " ",
                "--SYNTAX FOR RIGHT JOIN",
                "SELECT column_name1,",
                "column_name2",
                "FROM table_name",
                "RIGHT JOIN table_name1",
                "ON table_name1.column_name",
                " = table_name1.column_name ",
                " ",
                "--Example Query",
                "SELECT clientlname, ",
                "invoice_description, ",
                "invoice_dueby",
                "FROM invoices",
                "RIGHT JOIN clients",
                "ON clients.clientid ",
                "= invoices.clientid"
                });
            }

            else if (Code == "teacher_css")
            {
                codetoserve = new List<string>(new string[] {
                "/*EXAMPLE FOR DIFFERENT ",
                "STATE OF THE LINKS*/",
                " ",
                "/* unvisited link */",
                "a:link {",
                    "color: red;",
                "}",
                " ",
                "/* visited link */",
                "a:visited {",
                    "color: green;",
                "}",
                " ",
                "/* mouse over link */",
                "a:hover {",
                    "color: hotpink;",
                "}",
                " ",
                "/* selected link */",
                "a:active {",
                    "color: blue;",
                "}",
                " ",
                "EXAMPLE FOR TEXT DECORATION OF THE LINKS",
                "a:link {",
                    "text-decoration: none;",
                "}",
                " ",
                "a:visited {",
                    "text-decoration: none;",
                "}",
                " ",
                "a:hover {",
                    "text-decoration: underline;",
                "}",
                " ",
                "a:active {",
                    "text-decoration: underline;",
                "}",
                " ",
                "EXAMPLE FOR BACKGROUND OF THE LINKS",
                " ",
                "a:link {",
                    "background-color: yellow;",
                "}",
                " ",
                "a:visited {",
                    "background-color: cyan;",
                "}",
                " ",
                "a:hover {",
                    "background-color: lightgreen;",
                "}",
                " ",
                "a:active {",
                   "background-color: hotpink;",
                "}",
                });
            }



            else
            {
                codetoserve = new List<string>(new string[] {"There was no code found."});
            }

            //reference code from WEEK 9 Sample code
            int i = 1;
            foreach (string code_line in codetoserve)
            {
                coderow = codedata.NewRow();
                coderow[idx_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                codedata.Rows.Add(coderow);
            }


            DataView codeview = new DataView(codedata);
            return codeview;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DataView ds1 = CreateCodeSource();
            code_container.DataSource = ds1;

            code_container.DataBind();

        }
    }
    
}
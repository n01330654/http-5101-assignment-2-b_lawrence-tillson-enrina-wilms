﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CodeBox.ascx.cs" Inherits="assignment_2_b.usercontrols.CodeBox" %>
<ASP:DataGrid ID="code_container" runat="server" CssClass="code"
        GridLines="None" CellPadding="2"  >
        <HeaderStyle Font-Size="Large" />
        <ItemStyle Font-Size="Medium" HorizontalAlign="Left"/>     
    </ASP:DataGrid>
<div id="debug" runat="server" style="color:#fff;"></div>